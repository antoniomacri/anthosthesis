%%
%% This is file `anthosthesis.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% anthosthesis.dtx  (with options: `class')
%% 
%% This is `anthosthesis.cls', a modified version of the style
%% `classicthesis.sty' by André Miede.
%% Modifications as of November 2011 by Antonio Macrì.
%% 
%% Copyright (C) 2011 2012 Antonio Macrì <ing.antonio.macri@gmail.com>
%% Copyright (C) 2011 André Miede <http://www.miede.de>
%% 
%% License:
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program. If not, see <http://www.gnu.org/licenses/>.
%% 
%% Thanks to:
%% André Miede, Claudio Beccari, Enrico Gregorio, Lorenzo Pantieri
%% and to Tommaso Gordini who suggested the name of this new class.
%% 
%% Warning:
%% This is an alpha version: use it only for testing purposes or if
%% you want an overview on how it works. Options, commands, and
%% behaviour may change in future releases.
%% Avviso:
%% Questa è una versione alfa: usala solamente per testarla o se vuoi
%% un'anteprima del funzionamento della classe. Opzioni, comandi e
%% comportamenti potrebbero cambiare in futuro.
%% 
\def\thisclass{anthosthesis}
\def\thisproduct{AnthosThesis}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{\thisclass}
  [2012/02/06 v0.4
   A document class based on the ClassicThesis style]
\newif\ifant@tocaligned
\newif\ifant@eulerchapternumbers
\newif\ifant@drafting
\newif\ifant@linedheaders
\newif\ifant@nochapters
\newif\ifant@eulermath
\newif\ifant@parts
\newif\ifant@minionpro
\newif\ifant@minionprospacing
\newif\ifant@avpaper
\newif\ifant@bvpaper
\newif\ifant@dottedtoc
\newif\ifant@manychapters
\newif\ifant@floatperchapter
\def\ant@setup@baseclass{
  \PassOptionsToPackage{headinclude,footinclude}{typearea}
  \ifant@nochapters
    \def\ant@baseclass{scrartcl}
    \PassOptionsToClass{10pt,twoside,abstract=true,
      cleardoublepage=empty,numbers=noenddot,
      BCOR5mm,captions=tableheading,fleqn}{scrartcl}
  \else
    \def\ant@baseclass{scrbook}
    \PassOptionsToClass{10pt,
      cleardoublepage=empty,numbers=noenddot,
      BCOR5mm,captions=tableheading,fleqn}{scrbook}
  \fi
  \PassOptionsToPackage{fleqn}{amsmath}
  \let\ant@setup@baseclass\relax}
\DeclareOption{nochapters}{\ant@nochapterstrue}
\DeclareOption{a5paper}{\ant@avpapertrue}
\DeclareOption{b5paper}{\ant@bvpapertrue}
\DeclareOption{parts}{\ant@partstrue}
\DeclareOption{tocaligned}{\ant@tocalignedtrue}
\DeclareOption{eulerchapternumbers}{\ant@eulerchapternumberstrue}
\DeclareOption{drafting}{\ant@draftingtrue}
\DeclareOption{linedheaders}{\ant@linedheaderstrue}
\DeclareOption{eulermath}{\ant@eulermathtrue}
\DeclareOption{minionpro}{\ant@minionprotrue}
\DeclareOption{minionprospacing}{\ant@minionprospacingtrue}
\DeclareOption{dottedtoc}{\ant@dottedtoctrue}
\DeclareOption{manychapters}{\ant@manychapterstrue}
\DeclareOption{floatperchapter}{\ant@floatperchaptertrue}
\DeclareOption*{
  \ant@setup@baseclass
  \PassOptionsToClass{\CurrentOption}{\ant@baseclass}
}
\ProcessOptions\relax
\ant@setup@baseclass
\ifant@avpaper
  \PassOptionsToClass{a5paper}{\ant@baseclass}
\fi
\ifant@bvpaper
  \PassOptionsToClass{b5paper}{\ant@baseclass}
\fi
\LoadClass{\ant@baseclass}

\RequirePackage{ifxetex,ifluatex}
\newif\ifxetexorluatex
\ifxetex
  \xetexorluatextrue
\fi
\ifluatex
  \xetexorluatextrue
\fi

\ifant@minionprospacing
  \ifxetexorluatex
    \ClassInfoNoLine{\thisclass}{%
      Option "minionprospacing" is unused with xetex and luatex}
  \else
    \ClassInfoNoLine{\thisclass}{%
      Using option "minionprospacing".\MessageBreak
      This activates "minionpro" in general}
    \ant@minionprotrue
  \fi
\fi
\ifant@eulerchapternumbers
  \ifxetexorluatex
    \ClassInfoNoLine{\thisclass}{%
      Option "eulerchapternumbers" is unused with xetex and luatex}
  \fi
\fi
\ifant@nochapters
  \ifant@parts
    \ClassWarningNoLine{\thisclass}{%
      You cannot use "parts" at the same time\MessageBreak
      as "nochapters"}
  \fi
  \ifant@manychapters
    \ClassWarningNoLine{\thisclass}{%
      You cannot use "manychapters" at the same time\MessageBreak
      as "nochapters"}
  \fi
  \ifant@floatperchapter
    \ClassWarningNoLine{\thisclass}{%
      You cannot use "floatperchapter" at the same time\MessageBreak
      as "nochapters"}
  \fi
  \ClassInfoNoLine{\thisclass}{%
    Using option "nochapters" (probably for an\MessageBreak
    article). This turns off the options "linedheaders",\MessageBreak
    "eulerchapternumbers", "floatperchapter",\MessageBreak
    "manychapters", and "parts". Please be aware of that}
  \ant@linedheadersfalse
  \ant@eulerchapternumbersfalse
  \ant@partsfalse
  \ant@manychaptersfalse
  \ant@floatperchapterfalse
\fi

\PassOptionsToPackage{dvipsnames}{xcolor}
\RequirePackage{xcolor}
\definecolor{halfgray}{gray}{0.55}
\definecolor{webgreen}{rgb}{0,.5,0}
\definecolor{webbrown}{rgb}{.6,0,0}
\colorlet{partcolor}{Maroon}
\colorlet{chapternumbercolor}{halfgray}
\colorlet{urlcolor}{webbrown}
\colorlet{linkcolor}{RoyalBlue}
\colorlet{citecolor}{webgreen}

\ifxetexorluatex
  \newcommand{\setchapfont}[2][]{%
    \def\ant@chapnumfont{%
      \fontspec[#1]{#2}\fontsize{70}{0}\selectfont}}
  \def\ant@check@mainfont{
    \ifcsname zf@family@fontdef\f@family\endcsname\else
      \ClassError{\thisclass}{%
        Main font not selected}{%
        You must use the fontspec's command \string\setmainfont\space
        to set a valid font for text.}
    \fi}
  \def\ant@check@chapfont{
    \ifx\ant@chapnumfont\undefined
      \ClassError{\thisclass}{%
        Font for chapter numbers not selected}{%
        You must use \string\setchapfont\space to set a valid font for
        chapter numbers.}
    \fi}
  \AtBeginDocument{
    \ant@check@mainfont
    \ant@check@chapfont
  }
  \ifant@minionpro\else
    \linespread{1.05}
  \fi
\else
  \ifant@minionpro
    \ifant@eulermath
      \PassOptionsToPackage{onlytext}{MinionPro}
    \fi
    \PassOptionsToPackage{opticals,mathlf}{MinionPro}
    \RequirePackage{MinionPro}
  \else
    \PassOptionsToPackage{osf,sc}{mathpazo}
    \RequirePackage{mathpazo}
    \linespread{1.05}
  \fi
  \PassOptionsToPackage{scaled=0.85}{beramono}
  \RequirePackage{beramono}
  \ifant@eulermath
    \PassOptionsToPackage{euler-digits}{eulervm}
    \RequirePackage{eulervm}
  \fi
  \ifant@eulerchapternumbers
    \def\ant@chapnumfont{%
      \usefont{U}{zeur}{b}{n}\fontsize{70}{0}\selectfont}
  \else
    \def\ant@chapnumfont{%
      \fontfamily{pplj}\fontsize{70}{0}\selectfont}
  \fi
  \RequirePackage{microtype}
\fi

\ifant@avpaper
  \ifant@minionpro
    % Minion gets some extra sizes
    \ClassInfoNoLine{\thisclass}{A5 paper, MinionPro}
    \areaset[current]{278pt}{556pt}
    \setlength{\marginparwidth}{5em}
    \setlength{\marginparsep}{1.25em}
  \else
    % Palatino or other
    \ClassInfoNoLine{\thisclass}{A5 paper, Palatino}
    \ifcase\@ptsize\relax
      \areaset[current]{288pt}{525pt}
      \setlength{\marginparwidth}{30pt}
    \or
      \areaset[current]{288pt}{540pt}
      \setlength{\marginparwidth}{30pt}
    \or
      \areaset[current]{288pt}{555pt}
      \setlength{\marginparwidth}{30pt}
    \fi
    \setlength{\marginparsep}{1.25em}
  \fi
\else
\ifant@bvpaper
  \ifant@minionpro
    % Minion gets some extra sizes
    \ClassInfoNoLine{\thisclass}{B5 paper, MinionPro}
    \areaset[current]{278pt}{556pt}
    \setlength{\marginparwidth}{5em}
    \setlength{\marginparsep}{1.25em}
  \else
    % Palatino or other
    \ClassInfoNoLine{\thisclass}{B5 paper, Palatino}
    \ifcase\@ptsize\relax
      \areaset[current]{284pt}{630pt}
      \setlength{\marginparwidth}{90pt}
    \or
      \areaset[current]{284pt}{630pt}
      \setlength{\marginparwidth}{55pt}
    \or
      \areaset[current]{284pt}{630pt}
      \setlength{\marginparwidth}{45pt}
    \fi
    \setlength{\marginparsep}{1.25em}
  \fi
\else
  % A4
  \ifant@minionpro
    % Minion gets some extra sizes
    \ClassInfoNoLine{\thisclass}{A4 paper, MinionPro}
    \areaset[current]{312pt}{684pt}% 609 + 33 + 42 head \the\footskip
    \setlength{\marginparwidth}{7.5em}
    \setlength{\marginparsep}{2em}
  \else
    % Palatino or other
    \ClassInfoNoLine{\thisclass}{A4 paper, Palatino}
    \ifcase\@ptsize\relax
      \areaset[current]{338pt}{715pt}
      \setlength{\marginparwidth}{104pt}
    \or
      \areaset[current]{372pt}{753pt}
      \setlength{\marginparwidth}{69pt}
    \or
      \areaset[current]{405pt}{740pt}
      \setlength{\marginparwidth}{56pt}
    \fi
    \setlength{\marginparsep}{1.5em}
  \fi
\fi\fi


\clubpenalty=3000
\@clubpenalty=\clubpenalty
\widowpenalty=3000
\displaywidowpenalty=3000

\let\oldmarginpar\marginpar
\def\graffito@setup{%
   \slshape\footnotesize
   \parindent=0pt \lineskip=0pt \lineskiplimit=0pt
   \tolerance=2000 \hyphenpenalty=300 \exhyphenpenalty=300
   \doublehyphendemerits=100000
   \finalhyphendemerits=\doublehyphendemerits}
\DeclareRobustCommand{\graffito}[1]{\oldmarginpar
  [\graffito@setup\raggedleft\hspace{0pt}{#1}]
  {\graffito@setup\raggedright\hspace{0pt}{#1}}}
\def\marginpar{%
  \@ifnextchar[\oldmarginpar\graffito}

\RequirePackage{textcase}
\ifxetexorluatex
  \newcommand*{\spacedallcaps}[1]{\bgroup
      \addfontfeature{LetterSpace=16}%
      \MakeTextUppercase{#1}%
    \egroup}
  \newcommand*{\spacedlowsmallcaps}[1]{\bgroup
      \addfontfeature{LetterSpace=8}%
      \scshape\MakeTextLowercase{#1}%
    \egroup}
  \newcommand*{\unspaced}[1]{%
    \bgroup\normalfont#1\egroup}
\else
  \ifant@minionprospacing
    \ClassInfoNoLine{\thisclass}{%
      Using MinionPro's textssc for character spacing}
    \newcommand*{\spacedallcaps}[1]{%
      \textssc{\MakeTextUppercase{#1}}}
    \newcommand*{\spacedlowsmallcaps}[1]{%
      \textssc{\MakeTextLowercase{#1}}}
    \newcommand*{\unspaced}[1]{%
      \bgroup\normalfont #1\egroup}
  \else
    \ClassInfoNoLine{\thisclass}{%
      Using pdftex/microtype for character spacing.\MessageBreak
      Make sure your pdftex is version 1.40 or higher}
    \microtypesetup{expansion=false}
    \newcommand*{\spacedallcaps}[1]{%
      \textls*[160]{\MakeTextUppercase{#1}}}
    \newcommand*{\spacedlowsmallcaps}[1]{%
      \textls*[80]{\scshape\MakeTextLowercase{#1}}}
    \newcommand*{\unspaced}[1]{%
      \bgroup\normalfont\textls*[0]{#1}\egroup}
  \fi
\fi
\def\ant@setup@tocsmallcaps{
  \let\ant@old@contentsline\contentsline
  \def\contentsline##1##2{%
    \let\@tempa\@firstoftwo
    \expandafter\ifx\csname l@##1\endcsname\l@chapter\else
    \expandafter\ifx\csname l@##1\endcsname\l@part\else
      \let\@tempa\@secondoftwo
    \fi\fi
    \@tempa{%
      \ant@old@contentsline{##1}{\spacedlowsmallcaps{##2}}%
    }{%
      \ant@old@contentsline{##1}{##2}%
    }}
  \let\ant@old@numberline\numberline
  \renewcommand*\numberline[1]{%
    \unspaced{\ant@old@numberline{##1}}}
}
\AtBeginDocument{
  \ant@setup@tocsmallcaps
  \BeforePackage{hyperref}{
    \ClassError{\thisclass}{%
      Package hyperref loaded too late}{%
      The hyperref package was loaded after some configurations
      made by\MessageBreak \thisclass.cls. This may be the case
      if you loaded it inside\MessageBreak \string\AtBeginDocument.
      You should load hyperref explicitly in the preamble.}
  }
}

\PassOptionsToPackage{newparttoc}{titlesec}
\RequirePackage{titlesec}
\ifant@parts
  \titleformat{\part}[display]
    {\normalfont\centering\large}
    {\thispagestyle{empty}\partname~\MakeTextUppercase{\thepart}}
    {1em}{\color{partcolor}\spacedallcaps}
\fi
\ifant@linedheaders
  \titleformat{\chapter}[display]
    {\relax}
    {\raggedleft
     {\color{chapternumbercolor}\ant@chapnumfont\thechapter} \\ }
    {0pt}
    {\titlerule\vspace*{.9\baselineskip}\raggedright\spacedallcaps}
    [\normalsize\vspace*{.8\baselineskip}\titlerule]
\else
  \titleformat{\chapter}[display]
    {\relax}{\mbox{}\oldmarginpar{\vspace*{-3\baselineskip}\color
    {chapternumbercolor}\ant@chapnumfont\thechapter}}{0pt}
    {\raggedright\spacedallcaps}
    [\normalsize\vspace*{.8\baselineskip}\titlerule]
\fi
\titleformat{\section}
  {\relax}{\textsc{\MakeTextLowercase{\thesection}}}{1em}
  {\spacedlowsmallcaps}
\titleformat{\subsection}
  {\relax}{\textsc{\MakeTextLowercase{\thesubsection}}}{1em}
  {\normalsize\itshape}
\titleformat{\subsubsection}
  {\relax}{\textsc{\MakeTextLowercase{\thesubsubsection}}}{1em}
  {\normalsize\itshape}
\titleformat{\paragraph}[runin]
  {\normalfont\normalsize}{\theparagraph}{0pt}{\spacedlowsmallcaps}
\ifant@nochapters\relax\else
  \titlespacing*{\chapter}{0pt}{1\baselineskip}{1.2\baselineskip}
\fi
\titlespacing*{\section}{0pt}{1.25\baselineskip}{1\baselineskip}
\titlespacing*{\subsection}{0pt}{1.25\baselineskip}{1\baselineskip}
\titlespacing*{\paragraph}{0pt}{1\baselineskip}{1\baselineskip}
\renewcommand{\descriptionlabel}[1]{%
  \hspace*{\labelsep}\spacedlowsmallcaps{#1}}

\PassOptionsToPackage{titles}{tocloft}
\RequirePackage{tocloft}
\newlength{\newnumberwidth}
\settowidth{\newnumberwidth}{999}% yields overfull hbox warnings for pages>999
\cftsetpnumwidth{\newnumberwidth}
\newlength{\beforebibskip}
\setlength{\beforebibskip}{0em}
\newlength{\newchnumberwidth}
\settowidth{\newchnumberwidth}{.} % <--- tweak here if more space required
\ifant@manychapters
  \addtolength{\cftchapnumwidth}{\newchnumberwidth}
  \addtolength{\cftsecnumwidth}{\newchnumberwidth}
  \addtolength{\cftsecindent}{\newchnumberwidth}
  \addtolength{\cftsubsecnumwidth}{\newchnumberwidth}
  \addtolength{\cftsubsecindent}{2\newchnumberwidth}
  \addtolength{\cftsubsubsecnumwidth}{\newchnumberwidth}
\fi
\ifant@parts
  \renewcommand{\thepart}{\roman{part}}
  \setlength{\cftpartnumwidth}{\cftchapnumwidth}
  \renewcommand{\cftpartpresnum}{\scshape}% \MakeTextLowercase
  %\renewcommand{\cftpartaftersnum}{\cftchapaftersnum}
  %\renewcommand{\cftpartaftersnumb}{\quad}
  \renewcommand{\cftpartfont}{\color{partcolor}\normalfont}
  \renewcommand{\cftpartpagefont}{\normalfont}
  \ifant@dottedtoc\relax\else
    \renewcommand{\cftpartleader}{\hspace{1.5em}}
    \renewcommand{\cftpartafterpnum}{\cftparfillskip}
  \fi
  \setlength{\cftbeforepartskip}{1em}
  \setlength{\cftbeforechapskip}{0.1em}
  \setlength{\beforebibskip}{\cftbeforepartskip}
\fi
\ifant@nochapters\relax\else
  \renewcommand{\cftchappresnum}{\scshape\MakeTextLowercase}
  \renewcommand{\cftchapfont}{\normalfont}
  \renewcommand{\cftchappagefont}{\normalfont}
  \ifant@dottedtoc\relax\else
    \renewcommand{\cftchapleader}{\hspace{1.5em}}
    \renewcommand{\cftchapafterpnum}{\cftparfillskip}
  \fi
  %\setlength{\cftbeforechapskip}{0.1em}
\fi
\ifant@nochapters
  \setlength{\cftbeforesecskip}{.1em}
  \setlength{\beforebibskip}{1em}
\fi
\renewcommand{\cftsecpresnum}{\scshape\MakeTextLowercase}
\renewcommand{\cftsecfont}{\normalfont}
\renewcommand{\cftsecpagefont}{\normalfont}
\ifant@dottedtoc\relax\else
  \renewcommand{\cftsecleader}{\hspace{1.5em}}
  \renewcommand{\cftsecafterpnum}{\cftparfillskip}
\fi
\ifant@tocaligned
  \setlength{\cftsecindent}{\z@}
\fi
\renewcommand{\cftsubsecpresnum}{\scshape\MakeTextLowercase}
\renewcommand{\cftsubsecfont}{\normalfont}
\ifant@dottedtoc\relax\else
  \renewcommand{\cftsubsecleader}{\hspace{1.5em}}
  \renewcommand{\cftsubsecafterpnum}{\cftparfillskip}
\fi
\ifant@tocaligned
  \setlength{\cftsubsecindent}{\z@}
\fi
\renewcommand{\cftsubsubsecpresnum}{\scshape\MakeTextLowercase}
\renewcommand{\cftsubsubsecfont}{\normalfont}
\ifant@dottedtoc\relax\else
  \renewcommand{\cftsubsubsecleader}{\hspace{1.5em}}
  \renewcommand{\cftsubsubsecafterpnum}{\cftparfillskip}
\fi
\ifant@tocaligned
  \setlength{\cftsubsubsecindent}{\z@}
\fi
\newlength{\cftindents}
\settowidth{\cftindents}{\figurename~99}
\addtolength{\cftindents}{2.5em}
\newcommand\ant@nameredef[1]{%
  \expandafter\renewcommand\expandafter*\csname#1\endcsname}
\newcommand\setuplistof[2]{%
  \ant@nameredef{cft#1font}{\normalfont}
  \ant@nameredef{cft#1presnum}{#2~}
  \ant@nameredef{cft#1pagefont}{\normalfont}
  \ifant@dottedtoc\relax\else
    \ant@nameredef{cft#1leader}{\hspace{1.5em}}
    \ant@nameredef{cft#1afterpnum}{\cftparfillskip}
  \fi
  \setlength{\@nameuse{cft#1indent}}{\z@}
  \setlength{\@nameuse{cft#1numwidth}}{\cftindents}
}
\setuplistof{fig}{\figurename}
\setuplistof{tab}{\tablename}
\ifant@parts
  \AfterTOCHead[toc]{\vspace*{-\cftbeforepartskip}}
\else\ifant@nochapters\relax\else
  \AfterTOCHead[toc]{\vspace*{-\cftbeforechapskip}}
\fi\fi
\def\ant@setup@listings{
  \newlistof{listings}{lol}{\lstlistlistingname}
  \setuplistof{listings}{\lstlistingname}
  \let\l@lstlisting\l@listings
  \let\lstlistoflistings\listoflistings
}
\AfterPackage*{listings}
  {\ant@setup@listings}
\DeclareRobustCommand*{\deactivateaddvspace}{\let\addvspace\@gobble}
\AtBeginDocument{
  \addtocontents{lof}{\deactivateaddvspace}
  \addtocontents{lot}{\deactivateaddvspace}
  \@ifpackageloaded{listings}{
    \addtocontents{lol}{\deactivateaddvspace}}{}
  \BeforePackage{listings}{
    \ClassError{\thisclass}{%
      Package listings loaded too late}{%
      The listings package was loaded after \thisclass\ could
      configure \MessageBreak it. This may be the case if you loaded
      it inside \string\AtBeginDocument.\MessageBreak You should load
      listings explicitly in the preamble or you can use\MessageBreak
      etoolbox's command \string\AtEndPreamble\space instead.}
  }
}

\PassOptionsToPackage{automark}{scrpage2}
\RequirePackage{scrpage2}
\clearscrheadings
\setheadsepline{0pt}
\lehead{\mbox{\llap{\small\thepage\kern2em}%
  \spacedlowsmallcaps{\headmark}\hfil}}
\rohead{\mbox{\hfil{\spacedlowsmallcaps{\headmark}}%
  \rlap{\small\kern2em\thepage}}}
\renewcommand{\headfont}{\small}
\ifant@nochapters\relax\else
  \renewcommand{\chaptermarkformat}{}
\fi
\newcommand{\@sectionmarkformat}{%
  \thesection\enspace}
\ifant@nochapters\relax\else
  \let\ant@oldchapter=\chapter
  \renewcommand*\chapter{\secdef{\ant@@chapter}{\ant@@schapter}}
  \def\ant@@chapter{%
    \ant@oldchapter}
  \def\ant@@schapter#1{%
    \ant@oldchapter*{#1}%
    \chaptermark{#1}}
\fi
\let\ant@oldsection=\section
\renewcommand*\section{\secdef{\ant@@section}{\ant@@ssection}}
\def\ant@@section{%
  \let\sectionmarkformat\@sectionmarkformat
  \ant@oldsection}
\def\ant@@ssection#1{%
  \ant@oldsection*{#1}%
  \let\sectionmarkformat\relax
  \sectionmark{#1}}
\pagestyle{scrheadings}

\def\ant@setup@hyperref{
  \pdfstringdefDisableCommands{%
    \let\thepart\ignorespaces}
}
\AfterPackage*{hyperref}{
  \ant@setup@hyperref}
\AtBeginDocument{
  \@ifpackageloaded{hyperref}{
    \hypersetup{urlcolor=urlcolor,
      linkcolor=linkcolor,citecolor=citecolor}
  }{}
}

\deffootnote{0em}{0em}{\thefootnotemark\hspace*{.5em}}

\RequirePackage{scrtime} % time access
\newcommand{\finalVersionString}{}
\ifant@drafting
  \PassOptionsToPackage{draft}{prelim2e}
  \RequirePackage{prelim2e}
  \renewcommand{\PrelimWords}{\relax}
  \renewcommand{\PrelimText}{\footnotesize[\,\today\ at \thistime\,]}
\else
  \renewcommand{\finalVersionString}{%
    \emph{Final Version} as of \today\ at \thistime.}
\fi

\ifant@floatperchapter\else
  \AfterPackage*{listings}{
    % numberbychapter works in listings>=1.4
    \lstset{numberbychapter=false}
  }
  \ifant@nochapters\relax\else
    \RequirePackage{remreset}
    \@removefromreset{table}{chapter}
    \@removefromreset{figure}{chapter}
  \fi
  \renewcommand{\thetable}{\arabic{table}}
  \renewcommand{\thefigure}{\arabic{figure}}
\fi
\endinput
%%
%% End of file `anthosthesis.cls'.
